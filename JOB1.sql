CREATE TABLE cliente (
	nif		char(9)		not null,
	nombre		varchar(10) 	not null,
	apellido1	varchar(10) 	not null,
	apellido2	varchar(10) 	not null,
	direccion	varchar(20),	
	fechanacimiento	date		not null
);

ALTER TABLE cliente ADD CONSTRAINT cliente_pk PRIMARY KEY (nif);


CREATE TABLE producto (
	codigo		char(3)		not null,
	nombre		varchar(15)	not null,
	precio		double(4,2)	not null	
);

ALTER TABLE producto ADD CONSTRAINT producto_pk PRIMARY KEY (codigo);


CREATE TABLE proveedor (
	cif		char(9)		not null,
	nombre		varchar(10)	not null,
	direccion	varchar(20)	not null,
	producto.codigo	char(3)		not null
);

ALTER TABLE proveedor
	ADD CONSTRAINT proveedor_cod_producto FOREIGN KEY (producto.codigo)
		REFERENCES producto (codigo);

CREATE TABLE compra (
	cliente.nif		char(9)		not null,
	producto.codigo		char(3)		not null
);

ALTER TABLE compra
	ADD CONSTRAINT compra_pk PRIMARY KEY (cliente.nif, producto.codigo);

CREATE TABLE empresa (
	cif		char(9)		not null,
	nombre		varchar(15)	not null,
	direccion	varchar(15)	not null,
	cp		char(5)		not null,
	localidad	varchar(15)	not null,
	gerente		varchar(15)	not null
);

INSERT INTO empresa VALUES
	('16161618D', 'Mercadona', 'c/ Real, 108', '11300', 'La L�nea', 'Susana D�az');

INSERT INTO cliente VALUES 
	('12345678A', 'Mar�a', 'Gonz�lez', 'Arias', 'c/ Real, 4', '01/01/1990'),
	('23456781K', '�ngela', 'Garc�a', '�lvarez', 'c/ Real, 8', '02/01/1990'),
	('49392988D', 'Miguel', 'P�rez', 'Ruiz', 'c/ Georgia, 19', '10/11/1992'),
	('34155678L', 'Justa', 'Palacios', 'Cabo', 'c/ Argentina, 22', '11/07/1989'),
	('67885764B', 'Federico', 'Montes', 'S�nchez', 'c/ Suiza, 25', '20/09/1986')
;

INSERT INTO producto VALUES
	('001', 'libreta', '2'),
	('002', 'folios', '1'),
	('003', 'bol�grafo', '0.5'),
	('004', 'libro de texto', '5'),
	('005', 'l�piz', '0.3')
;

INSERT INTO proveedor VALUES
	('49392988Y', 'Ram�rez Logistics', 'c/ Alcal�, 1'),
	('93392988Y', 'D�ez Logistics', 'c/ Alcal�, 100'),
	('79392988Y', 'Rodr�guez Logistics', 'c/ Princesa, 75'),
	('95392988Y', 'International Logistics', 'c/ Gran V�a, 1'),
	('14392988Y', 'Logistics Logistics', 'c/ Alcal�, 700')
;

INSERT INTO compra VALUES
	('12345678A', '001'),
	('34155678L', '002'),
	('34155678L', '001'),
	('23456781K', '003'),
	('67885764B', '005'),
	('49392988D', '005')	
;
