CREATE DATABASE nombre;
DROP DATABASE tabla;
CREATE TABLE tabla;
DROP TABLE tabla;
ALTER TABLE tabla ADD campo tipodato;
ALTER TABLE tabla DROP COLUMN campo;
ALTER TABLE tabla ALTER COLUMN campo tipodato;
ALTER TABLE tabla MODIFY COLUMN campo tipodato;

--

CREATE DATABASE hospital_la_paz;

CREATE TABLE hospital(
	id 		INT		NOT NULL,
	nombre		CHAR (6)	NOT NULL,
	telefono	CHAR (9)	NOT NULL,
	direccion	VARCHAR(20)	NOT NULL
);


CREATE TABLE planta(
	id		INT		NOT NULL,
	nombre		VARCHAR(15)	NOT NULL	
);


CREATE TABLE habitacion(
	id		INT		NOT NULL	
);


CREATE TABLE cama(
	id		INT		NOT NULL,	
);


CREATE TABLE especialidad(
	id		INT		NOT NULL,
	nombre		VARCHAR(15)	NOT NULL
);


CREATE TABLE medico(
	numcolegiado	VARCHAR(10)	NOT NULL,
	dni		CHAR(9)		NOT NULL,
	nombre		VARCHAR(10)	NOT NULL,
	apellido1	VARCHAR(10)	NOT NULL,
	apellido2	VARCHAR(10)	NOT NULL,
	telefono	CHAR(9)		NOT NULL,
	estadocivil	CHAR(1)		NOT NULL,
	numhijos	CHAR(1)		NOT NULL
);


CREATE TABLE paciente(
	ss		CHAR(12)	NOT NULL,
	dni		CHAR(9)		NOT NULL,
	nombre		CHAR(10)	NOT NULL,
	apellido1	VARCHAR(10)	NOT NULL,
	apellido2	VARCHAR(10)	NOT NULL,
	telefono	CHAR(9)		NOT NULL,
	direccion	VARCHAR(20)	NOT NULL,
	estadocivil	CHAR(1)		NOT NULL,
	numhijos	CHAR(1)		NOT NULL
);


